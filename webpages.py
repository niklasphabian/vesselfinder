#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import bs4
import datetime
import requests
import time
import pytz
import re
import pandas
import numpy

utc = pytz.timezone('UTC')


def sign_coordinate(coordinate):
    if 'W' in coordinate or 'S' in coordinate:
        direction = -1.0
        coordinate = coordinate.rstrip('S')
        coordinate = coordinate.rstrip('W')
    elif 'E' in coordinate or 'N' in coordinate:
        direction = 1.0
        coordinate = coordinate.rstrip('N')
        coordinate = coordinate.rstrip('E')
    try:
        coordinate = float(coordinate) * direction
    except ValueError:
        coordinate = None
    return coordinate


class WebPage(object):
    def __init__(self):
        self.soup = None
        self.html = None
        self.json = None
        self.url = None
        self.payload = None
        self.session = requests.Session()
        user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0'
        self.session.headers['User-Agent'] = user_agent

    def to_file(self):
        with open('page.html', 'w') as outfile:
            outfile.write(self.html)

    def from_file(self):
        with open('page.html', 'r') as infile:
            self.soup = BeautifulSoup(infile.read(), 'lxml')

    def download(self, url=None):
        if url is None:
            url = self.url
        while True:
            try:
                ret = self.session.get(url=url, params=self.payload, timeout=10)
                break
            except requests.exceptions.ConnectionError:
                print('Download Failed. Retrying in 5 Seconds')
                time.sleep(5)
            except requests.exceptions.Timeout:
                print('Connection timed out after 10 seconds.. Retrying in 5 Seconds')
                time.sleep(5)
            except requests.exceptions.ChunkedEncodingError:
                print('Connection timed out after 10 seconds.. Retrying in 5 Seconds')
                time.sleep(5)
        if ret.status_code == 200:
            self.html = ret.text
            try:
                self.json = ret.json()
            except:
                self.json = None
            self.soup = BeautifulSoup(self.html, 'lxml')
            return True
        else:
            return False


class VesselPageVesselfinder(WebPage):
    def __init__(self, url):
        super(VesselPageVesselfinder, self).__init__()
        self.vessel_params = dict()
        self.url = url
        self.vessel_params['url'] = self.url
        self.vessel_params['ship_id'] = None

    def parse(self):
        self.get_date()
        self.get_name()
        self.get_report_date()
        self.get_type()
        self.get_country()
        self.get_location()
        self.get_speed()
        self.get_imo()
        self.get_mmsi()
        self.get_built_year()
        self.get_length()
        self.get_width()
        self.get_gt()
        self.get_in_range()

    def in_database(self):
        in_database = True
        error_tags = self.soup.find_all('p', 'col-md-8')
        if len(error_tags) > 0:
            error_string = error_tags[0].contents[0]
            if 'temporary not in our database' in error_string:
                in_database = False
        return in_database

    def get_date(self):
        timestamp = datetime.datetime.utcnow()
        timestamp = timestamp.replace(microsecond=0)
        timestamp = utc.localize(timestamp)
        self.vessel_params['timestamp'] = timestamp

    def get_name(self):
        name = self.soup.find('title').contents[0].split(' - ')[0]
        self.vessel_params['name'] = name

    def get_report_date(self):
        date_tag = self.soup.find(text=re.compile('last report.*', re.IGNORECASE))
        if date_tag is None:
            print('no report date in page')
            timestamp = None
        else:
            date_string = date_tag.parent.next_sibling.contents[0].strip()
            try:
                string_format = '%b %d, %Y %H:%M UTC'
                timestamp = datetime.datetime.strptime(date_string, string_format)
                timestamp = utc.localize(timestamp)
            except:
                print('could not extract timestamp from {}'.format(date_string))
                timestamp = None
        self.vessel_params['report_date'] = timestamp

    def get_type(self):
        if self.soup.find(text='AIS Type') is not None:
            vessel_type_tag = self.soup.find(text='AIS Type')
            vessel_type = vessel_type_tag.parent.next_sibling.contents[0].strip()
        elif self.soup.find(text='Ship type') is not None:
            vessel_type_tag = self.soup.find(text='Ship type')
            vessel_type = vessel_type_tag.parent.next_sibling.contents[0].strip()
        else:
            print('no type in in page')
            vessel_type = None
        self.vessel_params['vessel_type'] = vessel_type

    def get_country(self):
        country = None
        country_tags = self.soup.find_all(text='Flag')
        for country_tag in country_tags:
            if len(country_tag.parent.next_sibling.contents) > 0:
                country = country_tag.parent.next_sibling.contents[0].strip()
        if country is None:
            print('no country in page')
        self.vessel_params['country'] = country

    def get_location(self):
        coordiantes_tag = self.soup.find(text='Coordinates')
        if coordiantes_tag is None:
            print('no location in page')
            latitude = None
            longitude = None
        else:
            coordiantes_string = coordiantes_tag.parent.next_sibling.contents[0].strip()
            latitude = coordiantes_string.split('/')[0]
            longitude = coordiantes_string.split('/')[1]
            latitude = sign_coordinate(latitude)
            longitude = sign_coordinate(longitude)
        self.vessel_params['latitude'] = latitude
        self.vessel_params['longitude'] = longitude

    def get_speed(self):
        speed_tag = self.soup.find(text='Course / Speed')
        if speed_tag is None:
            print('no speed in page')
            speed = None
        else:
            speed_string = speed_tag.parent.next_sibling.contents[0].strip()
            speed = speed_string.split('/')[1].split('kn')[0].strip()
            try:
                speed = float(speed)
            except ValueError:
                print('could not extract speed from {}'.format(speed_string))
                speed = None
        self.vessel_params['speed'] = speed

    def get_imo(self):
        if self.soup.find(text='IMO / MMSI') is not None:
            imo_tag = self.soup.find(text='IMO / MMSI')
            imo_string = imo_tag.parent.next_sibling.contents[0].strip()
            imo = imo_string.split('/')[0].strip()
        elif self.soup.find(text='IMO number') is not None:
            imo_tag = self.soup.find(text='IMO number')
            imo_string = imo_tag.parent.next_sibling.contents[0].strip()
            imo = imo_string
        else:
            print('no imo in page')
            imo_string = ''
            imo = ''
        try:
            imo = int(imo)
        except ValueError:
            print('could not extract imo from {}'.format(imo_string))
            imo = None
        self.vessel_params['imo'] = imo

    def get_mmsi(self):
        if 'mmsi' in self.vessel_params:
            return
        if self.soup.find(text='IMO / MMSI') is not None:
            mmsi_tag = self.soup.find(text='IMO / MMSI')
            mmsi_string = mmsi_tag.parent.next_sibling.contents[0].strip()
            mmsi = mmsi_string.split('/')[1].strip()
            try:
                mmsi = int(mmsi)
            except ValueError:
                print('could not extract mmsi from {}'.format(mmsi_string))
                mmsi = None
        elif self.soup.find('h2', {'class': 'subtitle'}) is not None:
            mmsi_tag = self.soup.find('h2', {'class': 'subtitle'})
            mmsi_string = mmsi_tag.contents[0]
            if 'MMSI:' in mmsi_string:
                mmsi = mmsi_string.split('MMSI:')[1].strip()
                try:
                    mmsi = int(mmsi)
                except ValueError:
                    print('could not extract mmsi from {}'.format(mmsi_string))
                    mmsi = None
            else:
                mmsi = None
        else:
            print('no mmsi in page')
            mmsi = None
        self.vessel_params['mmsi'] = mmsi

    def get_built_year(self):
        if self.soup.find(text='Year of Built') is None:
            built = None
            print('no built info in page')
        else:
            built_string = self.soup.find(text='Year of Built').parent.next_sibling.contents[0].strip()
            try:
                built = int(built_string)
            except ValueError:
                print('could not extract built year from {}'.format(built_string))
                built = None
        self.vessel_params['built'] = built

    def get_length(self):
        if self.soup.find(text='Length / Beam') is not None:
            length_tag = self.soup.find(text='Length / Beam')
            length_string = length_tag.parent.next_sibling.contents[0].strip()
            length_string = length_string.split('/')[0].strip()
        elif self.soup.find(text='Length Overall (m)') is not None:
            length_tag = self.soup.find(text='Length Overall (m)')
            length_string = length_tag.parent.next_sibling.contents[0].strip()
        else:
            length_string = ''
        try:
            length = float(length_string)
        except ValueError:
            length = None
            print('could not extract length from {}'.format(length_string))
        except AttributeError:
            length = None
            print('could not extract length from {}'.format(length_string))
        self.vessel_params['length'] = length

    def get_width(self):
        if self.soup.find(text='Length / Beam') is not None:
            width_tag = self.soup.find(text='Length / Beam')
            width_string = width_tag.parent.next_sibling.contents[0].strip()
            if len(width_string.split('/')) > 1:
                width_string = width_string.split('/')[1].strip().split(' ')[0]
            else:
                width_string = None
        elif self.soup.find(text='Beam (m)') is not None:
            width_tag = self.soup.find(text='Beam (m)')
            width_string = width_tag.parent.next_sibling.contents[0].strip()
        else:
            print('no width in page')
            width_string = ''
        try:
            width = float(width_string)
        except (IndexError, ValueError, AttributeError, TypeError) as e:
            print('could not extract width from {}'.format(width_string))
            width = None
        self.vessel_params['width'] = width

    def get_gt(self):
        if self.soup.find(text='Gross Tonnage') is None:
            gt = None
            print('no gt info in page')
        else:
            gt_string = self.soup.find(text='Gross Tonnage').parent.next_sibling.contents[0].strip()
            try:
                gt = float(gt_string)
            except ValueError:
                gt = None
                print('could not extract gt from {}'.format(gt_string))
        self.vessel_params['gt'] = gt

    def get_in_range(self):
        if self.soup.find('span', {'class': 'icon-in-range-on'}) is not None:
            in_range = True
        elif self.soup.find('span', {'class': 'icon-in-range'}) is not None:
            in_range = False
        else:
            in_range = None
        self.vessel_params['in_range'] = in_range


class VesselOverviewPage(WebPage):
    def __init__(self, page_num):
        super(VesselOverviewPage, self).__init__()
        self.page_num = page_num
        self.payload = {'page': self.page_num}
        self.url = 'https://www.vesselfinder.com/vessels'

    def find_links(self):
        urls = []
        link_tags = self.soup.find_all('a', {'class': 'ship-link'})
        for link_tag in link_tags:
            rel_path = link_tag['href']
            url = 'https://www.vesselfinder.com' + rel_path
            urls.append(url)
        return urls

class VesselPageMarineTraffic(WebPage):
    def __init__(self, ship_id):
        super(VesselPageMarineTraffic, self).__init__()        
        self.vessel_params = dict()
        self.ship_id = ship_id
        timestamp = datetime.datetime.utcnow().replace(microsecond=0)
        timestamp = utc.localize(timestamp)
        self.vessel_params['timestamp'] = timestamp        
        self.vessel_params['ship_id']= ship_id
        self.vessel_params['mmsi'] = None
        self.vessel_params['imo'] = None
        self.vessel_params['name'] = None
        self.vessel_params['country'] = None
        self.vessel_params['vessel_type'] = None
        self.vessel_params['gt'] = None
        self.vessel_params['built'] = None
        self.vessel_params['length'] = None
        self.vessel_params['width'] = None
        self.vessel_params['report_date'] = None
        self.vessel_params['latitude'] = None
        self.vessel_params['longitude'] = None
        self.vessel_params['speed'] = None
        self.vessel_params['in_range'] = None        
        self.vessel_params['timeline'] = None
    
    def get_info(self):
        url = 'https://www.marinetraffic.com/en/vesselDetails/vesselInfo/shipid:{shipid}'
        url = url.format(shipid=self.ship_id)        
        print(url)
        success= self.download(url)
        if success:
            self.vessel_params['mmsi'] = self.json['mmsi']
            self.vessel_params['imo'] = self.json['imo']
            self.vessel_params['name'] = self.json['name']
            self.vessel_params['country'] = self.json['country']
            self.vessel_params['vessel_type'] = self.json['type']
            self.vessel_params['gt'] = self.json['grossTonnage']
            self.vessel_params['built'] = self.json['yearBuilt']
            self.vessel_params['length'] = self.json['length']
            self.vessel_params['width'] = self.json['breadth']
        
    def get_position(self):
        url = 'https://www.marinetraffic.com/en/vesselDetails/latestPosition/shipid:{shipid}'
        url = url.format(shipid=self.ship_id)
        success = self.download(url)        
        if success:
            self.vessel_params['report_date'] = datetime.datetime.utcfromtimestamp(self.json['lastPos'])
            self.vessel_params['latitude'] = self.json['lat']
            self.vessel_params['longitude'] = self.json['lon']
            self.vessel_params['speed'] = self.json['speed']
            self.vessel_params['in_range'] = self.json['isVesselInRange'] == 'true'
        
    def get_timeline(self):
        url = 'https://www.marinetraffic.com/en/vesselDetails/timeline/shipid:{shipid}'
        url = url.format(shipid=self.ship_id)
        success = self.download(url)
        if success:
            speed_array = self.json['vesselTimelineData']['speedArray']
            df = pandas.DataFrame(data=speed_array, columns=['timestamp', 'speed'])
            df['timestamp'] = pandas.to_datetime(df['timestamp'], unit='s')
            self.vessel_params['timeline'] = df


class VesselPageMarineTraffic_old(VesselPageVesselfinder):

    def __init__(self, mmsi=None, imo=None, ship_id=None):
        self.make_url(mmsi, imo, ship_id)
        super(VesselPageMarineTraffic, self).__init__(url=self.url)
        if mmsi is not None:
            self.vessel_params['mmsi'] = mmsi
        if imo is not None:
            self.vessel_params['imo'] = imo
        if ship_id is not None:
            self.vessel_params['ship_id'] = ship_id

    def get_name(self):
        name = self.soup.find('title').contents[0].split('Vessel details for: ')[1].split(' (')[0].strip()
        self.vessel_params['name'] = name

    def make_url(self, mmsi=None, imo=None, ship_id=None):
        if mmsi is not None:
            url_trunk = 'https://www.marinetraffic.com/en/ais/details/ships/mmsi:{mmsi}'
            self.url = url_trunk.format(mmsi=mmsi)
        elif imo is not None:
            url_trunk = 'https://www.marinetraffic.com/en/ais/details/ships/imo:{mmsi}'
            self.url = url_trunk.format(imo=imo)
        elif ship_id is not None:
            url_trunk = 'https://www.marinetraffic.com/en/ais/details/ships/shipid:{ship_id}'
            self.url = url_trunk.format(ship_id=ship_id)
        else:
            self.url = None

    def get_ship_id(self):
        if self.soup.find('link', rel='canonical') is not None:
            canonical_link = self.soup.find('link', rel='canonical')['href']
            self.vessel_params['ship_id'] = canonical_link.split('shipid:')[1].split('/')[0]
        else:
            self.vessel_params['ship_id'] = None

    def get_timeline(self):
        if 'ship_id' not in self.vessel_params:
            self.get_ship_id()
        timeline_page = TimeLinePage(self.vessel_params['ship_id'])
        timeline_page.session = self.session
        timeline_page.session.headers['Referer'] = self.url
        timeline_page.session.headers['X-Requested-With'] = 'XMLHttpRequest'
        timeline_page.session.headers['TE'] = 'Trailers'
        timeline_page.download()
        if timeline_page.is_timeline():
            self.vessel_params['timeline'] = timeline_page.get_timeline()
        else:
            self.vessel_params['timeline'] = None

    def get_length(self):
        value_string = self.get_parameter('Length Overall')
        try:
            length = float(value_string.split(' × ')[0].rstrip('m'))
        except ValueError:
            print('could not convert {} to length'.format(value_string))
            length = None
        self.vessel_params['length'] = length

    def get_width(self):
        value_string = self.get_parameter('Length Overall')
        if '×' not in value_string:
            self.vessel_params['width'] = None
            return
        try:
            width = float(value_string.split(' × ')[1].rstrip('m'))
        except ValueError:
            print('could not convert {} to width'.format(value_string))
            width = None
        self.vessel_params['width'] = width

    def get_gt(self):
        value_string = self.get_parameter('Gross Tonnage:')
        try:
            gt = float(value_string.strip())
        except ValueError:
            print('could not convert {} to gt'.format(value_string))
            gt = None
        self.vessel_params['gt'] = gt

    def get_type(self):
        vessel_type = self.get_parameter('AIS Vessel Type:')
        self.vessel_params['vessel_type'] = vessel_type

    def get_country(self):
        country = self.get_parameter('Flag:').split()[0]
        self.vessel_params['country'] = country

    def get_imo(self):
        if 'imo' in self.vessel_params:
            return
        imo_string = self.get_parameter('IMO:')
        try:
            imo = int(imo_string)
        except ValueError:
            print('could not extract imo from {}'.format(imo_string))
            imo = None
        self.vessel_params['imo'] = imo

    def get_mmsi(self):
        if 'mmsi' in self.vessel_params:
            return
        mmsi_string = self.get_parameter('MMSI:')
        try:
            mmsi = int(mmsi_string)
        except ValueError:
            print('could not extract mmsi from {}'.format(mmsi_string))
            mmsi = None
        self.vessel_params['mmsi'] = mmsi

    def get_built_year(self):
        built = self.get_parameter('Year Built:')
        try :
            built = int(built)
        except ValueError:
            print('could not convert {} to built year'.format(built))
            built = None
        self.vessel_params['built'] = built

    def get_in_range(self):
        in_range_str = self.get_parameter('Status:')
        if in_range_str == 'Active':
            in_range = True
        else:
            in_range = False
        self.vessel_params['in_range'] = in_range

    def get_report_date(self):
        timestamp = None
        date_string = self.get_parameter('Position Received')
        if type(date_string) is bs4.element.Tag:
            date_string = date_string['datetime']
        if 'ago' in date_string:
            date_string = date_string.split('ago')[1].replace('(', '').replace(')', '').strip()
        if 'now' in date_string:
            date_string = date_string.split('now')[1].replace('(', '').replace(')', '').strip()
        string_formats = ['%Y-%m-%d %H:%M UTC', '%Y-%m-%d %H:%M (UTC)',  '%Y-%m-%d %H:%M:%S']
        for string_format in string_formats :
            try:
                timestamp = datetime.datetime.strptime(date_string, string_format)
                timestamp = utc.localize(timestamp)
                break
            except ValueError:
                print('{} not convertible as {}'.format(date_string, string_format))
        self.vessel_params['report_date'] = timestamp

    def get_location(self):
        if self.soup.find('a', 'details_data_link') is not None:
            location_string = self.soup.find('a', 'details_data_link').contents[0]
            if '/' in location_string:
                latitude = location_string.split('/')[0].replace('°', '').strip()
                longitude = location_string.split('/')[1].replace('°', '').strip()
            else:
                # Rather than a location, we probably got a station ID
                latitude = None
                longitude = None
        else:
            latitude = None
            longitude = None
        self.vessel_params['latitude'] = latitude
        self.vessel_params['longitude'] = longitude

    def get_speed(self):
        speed_string = self.get_parameter('Speed/Course')
        speed_string = speed_string.split('kn')[0].strip()
        try:
            speed = float(speed_string)
        except ValueError:
            speed = None
        self.vessel_params['speed'] = speed

    def get_parameter(self, keyword):
        value_string = ''
        re_text = '.*{keyword}.*'.format(keyword=keyword)
        tag = self.soup.find(text=re.compile(re_text))
        if tag is not None:
            if tag.parent.parent.find('b') is not None:
                value_tag = tag.parent.parent.find('b')
            elif tag.parent.parent.find('strong') is not None:
                value_tag = tag.parent.parent.find('strong')
            else:
                value_tag = None
            if len(value_tag.contents) > 0:
                value_string = value_tag.contents[0]
        return value_string


def test_vesselfinder():
    url = 'https://www.vesselfinder.com/vessels/GONZALO-S-IMO-9327853-MMSI-0'
    vessel_page = VesselPageVesselfinder(url)
    vessel_page.download()
    vessel_page.parse()
    print(vessel_page.vessel_params)


def test_marinetraffic():
    page = VesselPageMarineTraffic(ship_id=9)    
    page.get_info()
    page.get_position()
    page.get_timeline()
    print(page.vessel_params)
    



if __name__ == '__main__':
    #test_vesselfinder()
    test_marinetraffic()



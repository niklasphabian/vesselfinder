import database
import matplotlib.pyplot as plt

db = database.PostgresDatabase(config_file='database.config', config_name='postgis')
vessels = database.VesselTable(database=db)
query = '''SELECT 
           FLOOR(mmsi/1000/1000)*1000*1000 AS mmsi, 
           COUNT(mmsi) as count from vessels 
           WHERE mmsi IS NOT NULL 
           GROUP BY floor(mmsi/1000/1000)'''

df = vessels.to_dataframe(query=query)

df.plot(kind='scatter', x='mmsi', y='count', figsize=(16,9), grid=True, logy=True)
print(df)

plt.savefig('mmsi_stats.png', dpi=150)

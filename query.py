import pandas


class Query:
    def __init__(self, query_path):
        self.text = None
        self.original_text = None
        self.load_query(query_path)

    def load_query(self, query_path):
        with open(query_path, 'rt') as source_file:
            self.text = source_file.read()
            self.original_text = self.text

    def format(self, **args):
        self.text = self.original_text.format(**args)

    def format_not_inplace(self, **args):
        return self.text.format(**args)

    def execute_commit(self, db):
        db.cursor.execute(self.text)
        db.commit()

    def execute_fetchall(self, db):
        db.cursor.execute(self.text)
        ret = db.cursor.fetchall()
        return ret

    def execute_fetchall_pandas(self, db):
        df = pandas.read_sql(sql=self.text, con=db.uri)
        return df

    def execute_fetchone(self, db):
        db.cursor.execute(self.text)
        ret = db.cursor.fetchone()[0]
        return ret

    def __str__(self):
        return self.text




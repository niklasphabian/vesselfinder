from webpages import VesselPageMarineTraffic
from logger import Logger
import database
import time


pg_db = database.PostgresDatabase('database.config', 'postgis')

schema='public'

pg_position_table = database.PositionTable(pg_db, schema=schema)
pg_vessel_table = database.VesselTable(pg_db, schema=schema)
pg_timeline_table = database.TimelineTable(pg_db, schema=schema)


def upsert_data(vessel_params):
    pg_vessel_table.upsert_vessel(ship_info=vessel_params)
    pg_position_table.insert_position(position_info=vessel_params)
    if vessel_params['timeline'] is not None:
        pg_timeline_table.upsert_timeline(mmsi=vessel_params['mmsi'], timeline=vessel_params['timeline'])
    pg_db.commit()


def get_vessel(ship_id):
    page = VesselPageMarineTraffic(ship_id=ship_id)
    page.get_position()
    page.get_timeline()
    page.get_info()
    if page.vessel_params['name'] is not None:
        print(page.vessel_params)
        upsert_data(page.vessel_params)
        page.vessel_params.pop('timeline', None)
    else:
        print('could not download')


def get_all():
    logger = Logger('log_mt.txt')
    last_shipid = logger.last_item()    
    ship_ids = range(last_shipid, 1000*1000) #pg_vessel_table.get_shipids(shipid_min=last_shipid)
    logger.set_tot(len(ship_ids))
    for ship_id in ship_ids:
        print('####################################')
        get_vessel(ship_id=ship_id)
        logger.update(downloaded_item=ship_id)
        time.sleep(1)
    logger.delete()


if __name__ == '__main__':
    #get_vessel(244630087)
    #while True:
    get_all()


source config_skunk.ini

scp -P $port $user@$host:$remoteFolder/database.py  $localFolder
scp -P $port $user@$host:$remoteFolder/query.py $localFolder
scp -P $port $user@$host:$remoteFolder/webpages.py $localFolder
scp -P $port $user@$host:$remoteFolder/logger.py $localFolder
scp -P $port $user@$host:$remoteFolder/main.py $localFolder

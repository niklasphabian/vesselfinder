source config_postgis.ini

localFolder=~/Dropbox/UCSB/ESM514/vesselfinder

scp -P $port $localFolder/database.py $user@$host:$remoteFolder
scp -P $port $localFolder/database.config $user@$host:$remoteFolder
scp -P $port $localFolder/query.py $user@$host:$remoteFolder
scp -P $port $localFolder/webpages.py $user@$host:$remoteFolder
scp -P $port $localFolder/logger.py $user@$host:$remoteFolder
scp -P $port $localFolder/crawl_vesselfinder.py $user@$host:$remoteFolder
scp -P $port $localFolder/crawl_marinetraffic.py $user@$host:$remoteFolder
scp -P $port $localFolder/requirements.txt $user@$host:$remoteFolder
scp -P $port $localFolder/database.config $user@$host:$remoteFolder

       
#scp -P $port $localFolder/log.txt $user@$host:$remoteFolder
#scp -P $port $localFolder/vessels.sqlite $user@$host:$remoteFolder


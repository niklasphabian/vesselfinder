from webpages import VesselOverviewPage, VesselPageVesselfinder
import database
from logger import Logger

pg_db = database.PostgresDatabase('database.config', 'postgis')

schema = 'public'
pg_position_table = database.PositionTable(pg_db, schema=schema)
pg_vessel_table = database.VesselTable(pg_db, schema=schema)


def get_ship(url):
    vessel_page = VesselPageVesselfinder(url)
    success = vessel_page.download()
    in_database = vessel_page.in_database()
    if success and in_database:
        vessel_page.parse()
        print(vessel_page.vessel_params)
        print('###################################################')
        upsert_data(vessel_page.vessel_params)


def upsert_data(vessel_params):
    pg_position_table.insert_position(vessel_params)
    pg_position_table.commit()
    pg_vessel_table.upsert_vessel(vessel_params)
    pg_vessel_table.commit()


def get_urls(page_num):
    vessel_overview = VesselOverviewPage(page_num)
    vessel_overview.download()
    urls = vessel_overview.find_links()
    return urls


def get_all_pages():
    log = Logger('log_vf.txt')
    start_page = log.last_item()
    end_page = 18872
    log.set_tot(end_page-start_page)    
    for page_num in range(start_page, end_page):
        urls = get_urls(page_num)
        for url in urls:
            print(url)
            get_ship(url)
        log.update(page_num)
    log.delete()


if __name__ == '__main__':
    while True:
        get_all_pages()



    


import sqlite3
import configparser
import psycopg2
import psycopg2.extras
import pandas
import sqlalchemy


class Database:
    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()


class PostgresDatabase(Database):
    def __init__(self, config_file, config_name):
        self.user = None
        self.pwd = None
        self.connection = None
        self.cursor = None
        self.dict_cursor = None
        self.host = None
        self.db_name = None
        self.uri = None
        self.load_config(config_file, config_name)
        self.connect()
        self.db_type = 'postgres'
        self.placeholder = '%s'
        self.bool_function = 'BOOL'

    def load_config(self, config_file, config_name):
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str
        config.read(config_file)
        self.host = config[config_name]['host']
        self.user = config[config_name]['user']
        self.pwd = config[config_name]['pwd']
        self.db_name = config[config_name]['database']
        self.uri = 'postgresql+psycopg2://{user}:{pwd}@{host}/{db_name}'
        self.uri = self.uri.format(user=self.user, pwd=self.pwd, host=self.host, db_name=self.db_name)

    def connect(self):
        connection_str = "host='{}' dbname='{}' user='{}' password='{}'".format(self.host, self.db_name, self.user, self.pwd)
        self.connection = psycopg2.connect(connection_str)
        self.cursor = self.connection.cursor()
        self.dict_cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def execute_query(self, query):
        self.cursor.execute(query)


class SQLiteDatabase(Database):
    def __init__(self, db_path):
        self.connection = sqlite3.connect(db_path)
        self.cursor = self.connection.cursor()
        self.uri = 'sqlite:///{db_path}'.format(db_path=db_path)
        self.db_type = 'sqlite'
        self.placeholder = '?'
        self.bool_function = ''


class DBTable:
    def __init__(self, database, schema=None):
        self.database = database
        self.schema = schema
        self.cursor = self.database.cursor
        if self.schema is not None:
            self.full_table_name = self.schema + '.' + self.table_name
        else:
            self.full_table_name = self.table_name

    def clear(self):
        query = 'DELETE FROM {table_name}'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def select_all(self):
        query = 'SELECT * FROM {table_name};'
        query = query.format(table_name=self.full_table_name)
        self.execute_query(query)

    def execute_query(self, query):
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)

    def drop(self):
        query = 'DROP TABLE IF EXISTS {table_name}'.format(table_name=self.full_table_name)
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def commit(self):
        self.database.commit()

    def from_dataframe(self, df):
        engine = sqlalchemy.create_engine(self.database.uri)
        df.to_sql(name=self.full_table_name, con=engine, schema=self.schema, if_exists='replace', index=True)

    def to_dataframe(self, query=None):
        if query is None:
            query = 'SELECT * FROM {table_name}'
        query = query.format(table_name=self.full_table_name)
        engine = sqlalchemy.create_engine(self.database.uri)
        df = pandas.read_sql(sql=query, con=engine)
        return df

    def to_list(self, query=None):
        if query is None:
            query = 'SELECT * FROM {table_name}'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        ret = self.database.cursor.fetchall()
        return ret

    def add_pkey(self):
        query = 'ALTER TABLE {full_table_name} ADD CONSTRAINT {table_name}_pkey PRIMARY KEY(idx);'
        query = query.format(full_table_name=self.full_table_name, table_name=self.table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def max_idx(self):
        query = 'SELECT max(idx) FROM {table_name}'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        ret = self.database.cursor.fetchone()
        return ret[0]

    def restart_sequence(self):
        max_idx = self.max_idx() + 1
        query = 'ALTER SEQUENCE {table_name}_idx_seq RESTART WITH {max_idx};'
        query = query.format(table_name=self.full_table_name, max_idx=max_idx)
        self.database.cursor.execute(query)
        self.database.commit()


class VesselTable(DBTable):
    table_name = 'vessels'

    def upsert_vessel(self, ship_info):
        if self.already_exists(ship_info):
            self.update_vessel(ship_info)
        else:
            self.insert(ship_info)

    def already_exists(self, ship_info):
        mmsi = ship_info['mmsi']
        imo = ship_info['imo']
        ship_id = ship_info['ship_id']
        ret = self.get_ship(mmsi=mmsi, imo=imo, ship_id=ship_id)
        if len(ret) > 0:
            return True
        else:
            return False

    def get_mmsis(self, mmsi_min=0):
        query = 'SELECT mmsi ' \
                'FROM {table_name} ' \
                'WHERE mmsi IS NOT NULL ' \
                'AND mmsi >= {val} ' \
                'ORDER BY mmsi;'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (mmsi_min,))
        ret = self.database.cursor.fetchall()
        return [i[0] for i in ret]

    def get_shipids(self, shipid_min=0):
        query = 'SELECT ship_id ' \
                'FROM {table_name} ' \
                'WHERE ship_id IS NOT NULL ' \
                'AND ship_id >= {val} ' \
                'ORDER BY ship_id;'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (shipid_min,))
        ret = self.database.cursor.fetchall()
        return [i[0] for i in ret]

    def delete_vessel(self, ship_info):
        if ship_info['mmsi']:
            mmsi = ship_info['mmsi']
            self.delete_by_mmsi(mmsi)
        else:
            imo = ship_info['imo']
            self.delete_by_imo(imo)

    def delete_by_imo(self, imo):
        query = 'DELETE FROM {table_name} ' \
                'WHERE imo = {val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (imo,))

    def delete_by_mmsi(self, mmsi):
        query = 'DELETE FROM {table_name} ' \
                'WHERE mmsi = {val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (mmsi,))

    def insert(self, ship_info):
        query = 'INSERT INTO {table_name} (mmsi, imo, name, country, vessel_type, gt, built, length, width, ship_id) ' \
                'VALUES ({val},{val},{val},{val},{val},{val},{val},{val},{val},{val})'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        mmsi = ship_info['mmsi']
        imo = ship_info['imo']
        name = ship_info['name']
        country = ship_info['country']
        vessel_type = ship_info['vessel_type']
        gt = ship_info['gt']
        built = ship_info['built']
        length = ship_info['length']
        width = ship_info['width']
        ship_id = ship_info['ship_id']
        self.database.cursor.execute(query, (mmsi, imo, name, country, vessel_type, gt, built, length, width, ship_id))

    def batch_insert(self, ship_list):
        query = 'INSERT INTO {table_name} (idx, mmsi, imo, name, vessel_type, gt, built, length, width, country, ship_id) ' \
                'VALUES ({val},{val},{val},{val},{val},{val},{val},{val},{val},{val},{val})'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.executemany(query, ship_list)
        self.database.commit()

    def batch_insert_pg(self, ret):
        query = 'INSERT INTO {table_name}(' \
                'idx, mmsi, imo, name, vessel_type, gt, built, length, width)'\
                'VALUES {val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        psycopg2.extras.execute_values(self.database.cursor, query, ret)
        
    def get_ship(self, mmsi=None, imo=None, ship_id=None):
        if mmsi:            
            query = 'SELECT * FROM {table_name} WHERE mmsi = {val};' 
            query = query.format(table_name=self.full_table_name, val=self.database.placeholder)                
            self.database.dict_cursor.execute(query, [mmsi])
        elif imo:
            query = 'SELECT * FROM {table_name} WHERE imo = {val};' 
            query = query.format(table_name=self.full_table_name, val=self.database.placeholder)                
            self.database.dict_cursor.execute(query, [imo])
        elif ship_id:
            query = 'SELECT * FROM {table_name} WHERE ship_id = {val};' 
            query = query.format(table_name=self.full_table_name, val=self.database.placeholder)                
            self.database.dict_cursor.execute(query, [ship_id])
        ret = self.database.dict_cursor.fetchall()
        return ret

    def update_vessel(self, ship_info):
        ret = self.get_ship(mmsi=ship_info['mmsi'], imo=ship_info['imo'], ship_id=ship_info['ship_id'])[0]
        idx = ret['idx']
        if ret['mmsi'] is None:
            self.update_column(idx=idx, column='mmsi', value=ship_info['mmsi'])
        if ret['imo'] is None:
            self.update_column(idx=idx, column='imo', value=ship_info['imo'])
        if ret['name'] is None:
            self.update_column(idx=idx, column='name', value=ship_info['name'])
        if ret['vessel_type'] is None:
            self.update_column(idx=idx7, column='vessel_type', value=ship_info['vessel_type'])
        if ret['gt'] is None:
            self.update_column(idx=idx, column='gt', value=ship_info['gt'])
        if ret['built'] is None:
            self.update_column(idx=idx, column='built', value=ship_info['built'])
        if ret['length'] is None:
            self.update_column(idx=idx, column='length', value=ship_info['length'])
        if ret['width'] is None:
            self.update_column(idx=idx, column='width', value=ship_info['width'])
        if ret['country'] is None:
            self.update_column(idx=idx, column='country', value=ship_info['country'])
        if ret['ship_id'] is None:
            self.update_column(idx=idx, column='ship_id', value=ship_info['ship_id'])
        
    def update_column(self, idx, column, value):
        query = 'UPDATE {table_name} SET {column}={val} WHERE idx={val} AND {column} IS NULL'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder, column=column)
        self.database.cursor.execute(query, (value, idx))
        
    def create(self):
        query = 'CREATE TABLE {table_name} (' \
                'idx serial,' \
                'mmsi integer,' \
                'imo integer,' \
                'name text,' \
                'vessel_type text,' \
                'gt double precision,' \
                'built integer,' \
                'length integer,' \
                'width integer,' \
                'country text,' \
                'ship_id, integer); '
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()


class PositionTable(DBTable):
    table_name = 'positions'

    def upsert_position(self, position_info):
        self.delete_position(position_info)
        self.insert_position(position_info)

    def delete_position(self, position_info):
        mmsi = position_info['mmsi']
        report_date = position_info['report_date']
        imo = position_info['imo']
        if mmsi:
            self.delete_by_mmsi(mmsi, report_date)
        else:
            self.delete_by_imo(imo, report_date)

    def delete_by_mmsi(self, mmsi, report_date):
        query = 'DELETE FROM {table_name} ' \
                'WHERE report_date = {val} ' \
                'AND mmsi = {val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (report_date, mmsi))
    
    def delete_by_imo(self, imo, report_date):
        query = 'DELETE FROM {table_name} ' \
                'WHERE report_date = {val} ' \
                'AND imo = {val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (report_date, imo))
        
    def clear_bad_data(self):
        query = 'DELETE FROM {table_name} WHERE "timestamp" not like "2%"'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def insert_position(self, position_info):
        query = 'INSERT INTO {table_name} ("timestamp", mmsi, imo, report_date, latitude, longitude, speed, in_range)' \
                'VALUES ({val},{val},{val},{val},{val},{val},{val},{val})'
        timestamp = position_info['timestamp']
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        mmsi = position_info['mmsi']
        imo = position_info['imo']
        report_date = position_info['report_date']
        latitude = position_info['latitude']
        longitude = position_info['longitude']
        speed = position_info['speed']
        in_range = position_info['in_range']
        self.database.cursor.execute(query, (timestamp, mmsi, imo, report_date, latitude, longitude, speed, in_range))

    def batch_insert(self, positions_list):
        query = 'INSERT INTO {table_name} ' \
                '(idx, mmsi, imo, report_date, latitude, longitude, speed, "timestamp", in_range) ' \
                'VALUES ({val},{val},{val},{val},{val},{val},{val},{val},{bool}({val}))'
        query = query.format(table_name=self.full_table_name,
                             val=self.database.placeholder,
                             bool=self.database.bool_function)
        self.database.cursor.executemany(query, positions_list)
        self.database.commit()

    def make_geometries_index(self):
        query = 'CREATE INDEX ON {table_name} USING gist(geom);'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def make_geometries(self):
        query = 'UPDATE {table_name} SET geom = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326) \
                WHERE geom IS NULL \
                AND latitude IS NOT NULL;'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def create(self):
        query = 'CREATE TABLE {table_name} (' \
                'idx serial,' \
                'mmsi integer,' \
                'imo integer,' \
                'report_date timestamp with time zone,' \
                'latitude double precision,' \
                'longitude double precision,' \
                'speed double precision,' \
                '"timestamp" timestamp with time zone, ' \
                'in_range boolean);'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def add_mmsi_index(self):
        query = 'CREATE INDEX positions_mmsi_idx ' \
                'ON {table_name} ' \
                'USING btree(mmsi);'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()


class TimelineTable(DBTable):
    table_name = 'timeline'

    def entry_already_exists(self, mmsi, timestamp):
        query = 'SELECT idx FROM {table_name} WHERE mmsi= {val} AND "timestamp"={val}'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.execute(query, (mmsi, timestamp))
        ret = self.database.cursor.fetchone()
        if ret is not None:
            return True
        else:
            return False

    def upsert_timeline(self, mmsi, timeline):
        query = 'INSERT INTO {table_name} (mmsi, "timestamp", speed) VALUES ({val}, {val}, {val})'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        for row in timeline.itertuples(index=False):
            if not self.entry_already_exists(mmsi, str(row.timestamp)):
                self.database.cursor.execute(query, (mmsi, str(row.timestamp), row.speed))

    def batch_insert(self, timeline_list):
        query = 'INSERT INTO {table_name} ' \
                '(idx, mmsi, speed, "timestamp") ' \
                'VALUES ({val},{val},{val},{val})'
        query = query.format(table_name=self.full_table_name, val=self.database.placeholder)
        self.database.cursor.executemany(query, timeline_list)
        self.database.commit()

    def create(self):
        query = 'CREATE TABLE {table_name} (' \
                'idx serial NOT NULL, ' \
                'mmsi integer, ' \
                'speed double precision, ' \
                '"timestamp" timestamp with time zone);'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()

    def add_mmsi_index(self):
        query = 'CREATE INDEX timeline_mmsi_idx ' \
                'ON {table_name} ' \
                'USING btree(mmsi);'
        query = query.format(table_name=self.full_table_name)
        self.database.cursor.execute(query)
        self.database.commit()





def test_update_vessel():
    pg_db = PostgresDatabase('database.config', 'postgis')
    schema='public'
    pg_vessel_table = VesselTable(pg_db, schema=schema)
    ship_info = {'mmsi': 228252600}
    print(pg_vessel_table.get_ship(imo=9450600))


if __name__ == '__main__':
    test_update_vessel()
    

import datetime
import os


class Logger:
    def __init__(self, file_name):
        self.start = datetime.datetime.now()
        self.counter = 0
        self.tot = None
        self.log_str = None
        self.file_name = file_name

    def set_tot(self, tot):
        self.tot = tot

    def update(self, downloaded_item):
        self.counter += 1
        self.update_log_str(downloaded_item)
        print(self.log_str)
        self.write_log()

    def now_str(self):
        now = datetime.datetime.now()
        now_str = now.strftime('%Y-%m-%d %H:%M:%S')
        return now_str

    def time_left_str(self):
        now = datetime.datetime.now()
        time_left = (now - self.start) / self.counter * (self.tot - self.counter)
        time_left_str = '{days} days, {hours}:{minutes}'
        hours_left, rem = divmod(time_left.seconds, 3600)
        minutes_left, seconds_left = divmod(rem, 60)
        time_left_str = time_left_str.format(days=time_left.days, hours=hours_left, minutes=minutes_left)
        return time_left_str

    def update_log_str(self, downloaded_item):
        time_left_str = self.time_left_str()
        now_str = self.now_str()
        remaining = self.tot - self.counter
        self.log_str = '{now}: Downloaded {page} ({remaining} remaining) time_left: {time_left}'
        self.log_str = self.log_str.format(now=now_str, page=downloaded_item, remaining=remaining, time_left=time_left_str)

    def write_log(self):
        with open(self.file_name, 'a') as log:
            log.writelines(self.log_str + '\n')

    def last_item(self):
        if os.path.isfile(self.file_name):
            return self.read_last_item()
        else:
            self.create_log()
            return 1

    def read_last_item(self):
        with open(self.file_name, 'r') as log:
            last_row = log.readlines()[-2]
            page = int(last_row.split('Downloaded ')[1].split(' ')[0])
            return page

    def create_log(self):
        with open(self.file_name, 'w'):
            pass
          
    def delete(self):
      os.remove(self.file_name)


